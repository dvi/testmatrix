//
//  AppDelegate.h
//  testMatrix
//
//  Created by sashadiv on 15/12/15.
//  Copyright © 2015 sashadiv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

