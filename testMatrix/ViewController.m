//
//  ViewController.m
//  testMatrix
//
//  Created by sashadiv on 15/12/15.
//  Copyright © 2015 sashadiv. All rights reserved.
//

#import "ViewController.h"

NSString *const kOneTwoTrip = @"OneTwoTrip";

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *oneTwoTripInput = @[@"O", @"N", @"E", @"T", @"W", @"O", @"T", @"R", @"I", @"P"];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:kOneTwoTrip ofType:@"txt"];
    NSStringEncoding encoding;
    NSString* content;
    NSArray *sizeArray;
    NSMutableArray *matrixArray = [NSMutableArray array];
    
    if(path) {
        content = [NSString stringWithContentsOfFile:path  usedEncoding:&encoding  error:NULL];
    }
    
    if (content) {
        NSArray *rowArray   = [content componentsSeparatedByString:@"\n"];
        NSString *firstRow  = [rowArray firstObject];
        sizeArray  = [firstRow componentsSeparatedByString:@" "];
        matrixArray = [rowArray mutableCopy];
        [matrixArray removeObject:firstRow];
    }
    
    NSMutableArray *pathArray = [NSMutableArray array];
    for (NSString *string in matrixArray) {
        NSMutableArray *arrayOfChars = [NSMutableArray array];
        for (int i = 0; i < [string length]; i++) {
            [arrayOfChars addObject:[NSString stringWithFormat:@"%c", [string characterAtIndex:i]]];
        }
        [pathArray addObject:arrayOfChars];
    }
    
    NSLog(@"Start check path");
    BOOL resultPath = [self checkPath:pathArray withInput:oneTwoTripInput sizeArray:sizeArray];
    if (!resultPath) {
        NSLog(@"Impossible");
    }
    
    NSLog(@"Start check chars extis");
    BOOL resultExist = [self checkCharExits:pathArray withInput:kOneTwoTrip sizeArray:sizeArray];
    if (!resultExist) {
        NSLog(@"Impossible");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)checkCharExits:(NSArray *)pathArray withInput:(NSString *)input sizeArray:(NSArray *)sizeArray {
    
    int n  = [[sizeArray firstObject]intValue];
    int m  = [[sizeArray lastObject] intValue];
    int i = 0, j = 0, c = 0;
    NSUInteger N = input.length;
    
    NSMutableArray* cacheArray = [[NSMutableArray alloc] init];
    while (cacheArray.count < n) {
        [cacheArray addObject:[[NSMutableArray alloc] init]];
    }
    
    for(int i = 0; i < n; i++) {
        for (int j = 0; j< m; j++) {
            cacheArray[i][j]  = @0;
        }
    }
    
    while (i < n) {
        while (j < m) {
            while (c < N && ![cacheArray[i][j] boolValue]) {
                if ([input localizedCaseInsensitiveContainsString:pathArray[i][j]] && ![cacheArray[i][j] boolValue]) {
                    cacheArray[i][j] = @1;
                    NSLog(@"%@ - (%d, %d)", pathArray[i][j], i, j);
                }
                c++;
            }
            j++;
            c = 0;
        }
        i++;
        j = 0;
    }
    
    NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:input.length];
    for (NSArray *array in cacheArray) {
        [tempArray addObjectsFromArray:array];
    }
    
    NSArray *checkArray = [tempArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSNumber *number, NSDictionary<NSString *,id> * _Nullable bindings) {
        return [number boolValue];
    }]];
    
    return checkArray.count == input.length;
}

- (BOOL)checkPath:(NSArray*)pathArray withInput:(NSArray*)input sizeArray:(NSArray *)sizeArray {
    
    int n  = [[sizeArray firstObject]intValue];
    int m  = [[sizeArray lastObject] intValue];
    
    NSMutableArray* cacheArray = [[NSMutableArray alloc] init];
    while (cacheArray.count < n) {
        [cacheArray addObject:[[NSMutableArray alloc] init]];
    }
    
    for(int i = 0; i < n; i++) {
        for (int j = 0; j< m; j++) {
            cacheArray[i][j]  = @0;
        }
    }
    
    BOOL res = NO;
    
    int i = 0, j = 0;
    
    while (i < n) {
        while (j < m) {
            if(input[0] && [input[0] compare:pathArray[i][j] options:NSCaseInsensitiveSearch] == NSOrderedSame) {
                cacheArray[i][j] = @1;
                NSLog(@"%@ - (%d, %d)", pathArray[i][j], i, j);
                res = [self doesPathExist:input dictionaryArray:pathArray startIndexX:i andStartIndexY:j compareIndex:1 andMarkerTable:cacheArray];
                // Reset the test option incase the chosen option wasn't success
                cacheArray[i][j] = [NSNumber numberWithBool:res];
            }
            j++;
        }
        i++;
    }
    return res;
}

- (BOOL)doesPathExist:(NSArray *)input dictionaryArray:(NSArray*)pathArray startIndexX:(int)x andStartIndexY:(int)y compareIndex:(int)c andMarkerTable:(NSMutableArray *) markerTable {
    
    NSUInteger n = markerTable.count;
    NSUInteger m = ((NSArray*)markerTable[0]).count;
    NSUInteger N = input.count;
    
    if (c == N) {
        return YES;
    }else {
        BOOL result = NO;
        while (c < N) {
            @autoreleasepool {
                int left = y--;
                int right = y++;
                int top = x--;
                int bottom = x++;
                
                if (left >= 0 && ![markerTable[x][left] boolValue]) {
                    if(pathArray[x][left] && [pathArray[x][left] compare:input[c] options:NSCaseInsensitiveSearch] == NSOrderedSame){
                        markerTable[x][left] = @1;
                        NSLog(@"%@ - (%d, %d)", pathArray[x][left], x, left);
                        result = [self doesPathExist:input dictionaryArray:pathArray startIndexX:x andStartIndexY:left compareIndex:c++ andMarkerTable:markerTable];
                        markerTable[x][left] = [NSNumber numberWithBool:result];
                        return result;
                    }
                }
                if (right < m && ![markerTable[x][right] boolValue]) {
                    if(pathArray[x][right] && [pathArray[x][right] compare:input[c] options:NSCaseInsensitiveSearch] == NSOrderedSame) {
                        markerTable[x][right] = @1;
                        NSLog(@"%@ - (%d, %d)", pathArray[x][right], x, right);
                        result = [self doesPathExist:input dictionaryArray:pathArray startIndexX:x andStartIndexY:right compareIndex:c++ andMarkerTable:markerTable];
                        markerTable[x][right] = [NSNumber numberWithBool:result];
                        return result;
                    }
                }
                if(top >= 0 && ![markerTable[top][y] boolValue]) {
                    if(pathArray[top][y] && [pathArray[top][y] compare:input[c] options:NSCaseInsensitiveSearch] == NSOrderedSame) {
                        markerTable[top][y] = @1;
                        NSLog(@"%@ - (%d, %d)", pathArray[top][y], top, y);
                        result = [self doesPathExist:input dictionaryArray:pathArray startIndexX:top andStartIndexY:y compareIndex:c++ andMarkerTable:markerTable];
                        markerTable[top][y] = [NSNumber numberWithBool:result];
                        return result;
                    }
                }
                if(bottom < n && ![markerTable[bottom][y] boolValue]) {
                    if(pathArray[bottom][y] && [pathArray[bottom][y] compare:input[c] options:NSCaseInsensitiveSearch] == NSOrderedSame) {
                        markerTable[bottom][y] = @1;
                        NSLog(@"%@ - (%d, %d)", pathArray[bottom][y], bottom, y);
                        result = [self doesPathExist:input dictionaryArray:pathArray startIndexX:bottom andStartIndexY:y compareIndex:c++ andMarkerTable:markerTable];
                        markerTable[bottom][y] = [NSNumber numberWithBool:result];
                        return result;
                    }
                }
                c++;
            }

        }
        return NO;
    }
}

@end
